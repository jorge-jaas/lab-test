# LabTest

Este proyecto fue generado por [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Clonar repositorio

En linea de comando en una carpeta vacía correr `git clone https://jorge-jaas@bitbucket.org/jorge-jaas/lab-test.git`

## Instalar dependencias

En terminal dentro de la carpeta del proyecto ejecutar `npm i` y esperar que descargue las librerías.

## CORS

Para usar esta API, debemos abordar un problema curioso llamado CORS, Cross-Origin Resource Sharing.

Esta es una medida de seguridad implementada en todos los navegadores que le impide usar una API de una manera potencialmente no solicitada y la mayoría de las API, incluida esta: [https://www.indecon.online/] la tienen activa.

Debido a CORS si intentamos hacer una solicitud a la URL de la API con la biblioteca del cliente Http, el navegador emitiría un error CORS. Este no sería problema si el servicio entragara respuesta en [JSONP](https://es.wikipedia.org/wiki/JSONP) Implementación que no tiene. Que a su vez es una forma Segura y válida.

Para este desarrollo se optó por usar una extensión que omite este error. ¿Como hacerlo? 
 
 + Si usa Chrome se debe instalar una extención [Allow CORS: Access-Control-Allow-Origin 0.1.3]. Luego de instalada y habilitada se podrá hacer el siguiente paso

 + Si está utilizando Safari, comience habilitando el menú Desarrollar desde Preferencias → Avanzado. Cuando haya terminado, es posible que deba reiniciar Safari. En el menú Desarrollar, asegúrese de que la opción Desactivar restricciones de origen cruzado esté marcada, deberá actualizar la página para que los cambios surtan efecto.


## Servidor de desarrollo

Correr `ng serve` para servidor dev. Navegar a `http://localhost:4200/`.  La applicación automaticamente recargará si tu cambias alguno de los archivos fuentes.

## Producción

Correr `ng build` para construir el proyecto. Los artefactos construídos se guardarán en la carpeta `dist/`. Use la bandera `--prod` para ambiente productivo.

## Corriendo unit tests

Correr `ng test` para ejecutar las pruebas unitarias via [Karma](https://karma-runner.github.io). El metodo principal que se está probando es [getValueData] dentro de `service/` que obtiene la data del servicio, fundamental para la app. Genera una llamada [Mock] y Debería buscar datos como una observable.

## Ayuda adicional

Revisar [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
