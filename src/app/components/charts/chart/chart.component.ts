import { Component } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DialogOverview } from '../../popup/dialog-overview';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent {

  loading: boolean = false;
  showCharts: boolean = false;
  reformattedArray = [];
  results: any[] = [];
  private dataService: DataService;
  indicators: any[]= ['cobre','dolar','euro','ipc','ivp','oro','plata','uf','utm','yen'];
  multi: any[];
  view: any[] = [700, 300];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Fecha';
  yAxisLabel: string = 'Valor';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#7aa3e5', '#a8385d', '#aae3f5']
  };

  constructor(dataService: DataService, public dialog: MatDialog) {
    this.dataService = dataService;
  }

  searchIndicator(event){
    this.results = [];
    this.showCharts = false;
    this.loading = true;
    this.dataService.getValueData(event.value)
    .pipe(map(
      item => {
        return {'name': (event.value).toUpperCase(), 'series':
          Object.keys(item.values).map(function(key) {
            let d : Date = new Date(Number(key)*1000);
            return {'name':d, 'value': item.values[key]};
          })
        }
      })
    ).subscribe(data => {
      this.results.push(data);
      this.showCharts = true;
      this.loading = false;
    }
    ,
    error => {
      console.log("error: ", error);
      this.openDialog();
    });
  }

  onSelect(event) {
    console.log(event);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverview, {
      width: '350px'
    });
  }

}
