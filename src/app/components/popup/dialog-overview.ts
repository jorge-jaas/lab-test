import { Component, Inject } from "@angular/core";
import {
  MatDialogRef
} from "@angular/material/dialog";

@Component({
  selector: 'dialog-overview',
  templateUrl: 'dialog-overview.html'
})
export class DialogOverview {

  constructor(
    public dialogRef: MatDialogRef<DialogOverview>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
