import { TestBed, async, inject } from '@angular/core/testing';
import { DataService } from './data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('DataService', () => {
  let service: DataService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule],
      providers: [
        DataService
      ],
    });
    service = TestBed.inject(DataService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it(`should fetch indicator as an Observable`, async(inject([HttpTestingController, DataService],
    (httpClient: HttpTestingController, postService: DataService) => {

      const item = [
        {
          "key":"ipc",
          "name":"Indice de Precios al Consumidor (Var. c/r al período anterior)",
          "unit":"porcentual",
          "values":{
             "852076800":7.2,
             "854755200":4.9,
             "857174400":4.8,
             "859852800":5.8,
             "862444800":8.0,
             "865123200":6.5,
             "867715200":7.5
          }
        }
      ];


      service.getValueData('cobre')
        .subscribe((posts: any) => {
          expect(posts.length).toBe(1);
        });

      let req = httpMock.expectOne('http://www.indecon.online/values/cobre');
      expect(req.request.method).toBe("GET");

      req.flush(item);
      httpMock.verify();

    })));
});
